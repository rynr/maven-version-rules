maven-version-rules
===================

These version rules are meant to ignore all alpha, beta or release candidate
versions.

To use these version rules, add the version plugin to your maven file like:

```xml
<project>
	<!-- … -->
	<build>
		<plugins>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>versions-maven-plugin</artifactId>
				<version>2.7</version>
				<configuration>
					<rulesUri>classpath:///maven-version-rules.xml</rulesUri>
				</configuration>
				<dependencies>
					<dependency>
						<groupId>org.rjung.util</groupId>
						<artifactId>maven-version-rules</artifactId>
						<version>1.0</version>
					</dependency>
				</dependencies>
			</plugin>
		</plugins>
	</build>
</project>

```
